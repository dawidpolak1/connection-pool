public class PoolConfig {
    public static final int INITIAL_POOL_SIZE = 10;
    public static final int MAX_POOL_SIZE = 50;
    public static final int NUMBER_OF_QUERIES = 50;
    public static final int VALIDATION_INTERVAL = 60;
    public static final int MAX_TIMEOUT = 1;
    public static final String SQL = "INSERT INTO test(body) VALUES('abc')";
}
