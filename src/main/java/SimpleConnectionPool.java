import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SimpleConnectionPool implements ConnectionPool {

    private String url;
    private String user;
    private String password;
    private List<Connection> pool;
    private ScheduledExecutorService validationExecutor;
    private static final String CONNECTION_PROPERTIES = "connection.properties";

    public SimpleConnectionPool() {
        this.pool = new ArrayList<>();
        loadConnectionData();
        initializeConnectionPool();
    }

    private void initializeConnectionPool() {
        try {
            for (int i = 0; i < PoolConfig.INITIAL_POOL_SIZE; i++) {
                pool.add(createConnection(url, user, password));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection createConnection(String url, String user, String password) throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    private void loadConnectionData() {
        try (InputStream input =
                     ClassLoader.getSystemResourceAsStream(CONNECTION_PROPERTIES)) {
            Properties properties = new Properties();
            properties.load(input);
            url = properties.getProperty("url");
            user = properties.getProperty("user");
            password = properties.getProperty("password");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (pool.size() < PoolConfig.MAX_POOL_SIZE) {
            Connection connection = createConnection(url, user, password);
            pool.add(connection);
            return connection;
        } else {
            Iterator<Connection> iterator = pool.iterator();
            while (iterator.hasNext()) {
                Connection connection = iterator.next();
                try {
                    if (connection.isValid(PoolConfig.MAX_TIMEOUT)) {
                        iterator.remove();
                        return connection;
                    } else {
                        iterator.remove();
                        closeConnection(connection);
                    }
                } catch (SQLException e) {
                    iterator.remove();
                    closeConnection(connection);
                    //TODO: Log
                }
            }
        }

        throw new SQLException("Reached maximum pool size. No available connections!");
    }

    private void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO: Log
        }
    }


    @Override
    public void releaseConnection(Connection connection) throws SQLException {
    }

    public void startValidationExecutor() {
        validationExecutor = Executors.newSingleThreadScheduledExecutor();
        validationExecutor.scheduleAtFixedRate(
                this::validateConnections,
                PoolConfig.VALIDATION_INTERVAL, PoolConfig.VALIDATION_INTERVAL, TimeUnit.SECONDS);
    }

    @Override
    public void validateConnections() {
        List<Connection> invalidConnections = new ArrayList<>();
        for (Connection connection : pool) {
            try {
                if (!connection.isValid(PoolConfig.MAX_TIMEOUT)) {
                    invalidConnections.add(connection);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        pool.removeAll(invalidConnections);
    }

    @Override
    public int getSize() {
        return pool.size();
    }
}
