package app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.time.Instant;

public class ConnectAndCloseEveryTime {
    private static final String TITLE = ":: Connecting/closing the database connection every time ::";

    public static void main(String[] args) {
//        System.out.print(TITLE);
//
//        System.out.printf("%nSTART");
//        Instant start = Instant.now();
//
//        for (int i = 0; i < ConnectionProvider.NUMBER_OF_QUERIES; i++) {
//            try (Connection connection = DriverManager.getConnection(
//                    ConnectionProvider.URL, ConnectionProvider.USER, ConnectionProvider.PASSWORD);
//                 Statement statement = connection.createStatement()
//                ) {
//                    statement.execute(ConnectionProvider.SQL);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//        }
//
//        System.out.printf("%nEND%n");
//        Instant end = Instant.now();
//        Duration duration = Duration.between(start, end);
//
//        System.out.println(":: DURATION: " + duration.getSeconds() + " seconds ::");

        // test 1: 284 seconds
        // test 2: 275 seconds
        // test 3: 281 seconds
    }
}
