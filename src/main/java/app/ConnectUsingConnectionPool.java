package app;

import java.sql.SQLException;

public class ConnectUsingConnectionPool {

    public static void main(String[] args) throws SQLException, InterruptedException {
//        ExecutorService executorService = Executors.newFixedThreadPool(50);
//
//        Instant start = Instant.now();
//
//        for (int i = 0; i < NUMBER_OF_QUERIES; i++) {
//            executorService.execute(connectionPool);
//        }
//
//        Instant stop = Instant.now();
//        executorService.shutdown();
//        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
//        Duration duration = Duration.between(start, stop);
//        System.out.println("Duration while connect to database using connection pool: "
//                + duration.toMillis() + " milliseconds");
    }
}

/* 1. pool can keep up to a maximum of 50 connections
2. If the pool is empty - return a massage says: It is not possible to get a connection at this moment. The pool is empty
3. If an error occurred during connection to database - this connection should be removed and create me new connection to database

 */
